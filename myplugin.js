(function (factory) {
    /* Global define */
    if (typeof define === 'function' && define.amd) {
      // AMD. Register as an anonymous module.
      define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
      // Node/CommonJS
      module.exports = factory(require('jquery'));
    } else {
      // Browser globals
      factory(window.jQuery);
    }
  }(function ($) {
    $(document).ready(function() {
        $('#summernote').summernote({
            buttons: {
              youtube: YoutubeButton
            },
            dialogsInBody: true,
            dialogsFade: false,  // Add fade effect on dialogs
          toolbar: [
            [ ['youtube']],
            // ['custom', ['examplePlugin']],
             ['table', ['table']],
             ['insert', ['link', 'picture']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
             ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
             ['height', ['height']]
             
          ]
           
          
        }) //CLOSE $SUMMERNOTE
      
        });//close parent

})
)
var YoutubeButton = function (context) {
    var ui = $.summernote.ui;
  
    // create button
    var button = ui.button({
      contents: '<i class="fab fa-youtube"></i>',
      tooltip: 'Youtube',
      click: function () {
        var person = prompt("Please enter your name", "Harry Potter");

        if (person != null) {

        // invoke insertText method with 'hello' on editor module.
        context.invoke('editor.insertText', person);
                      }


  }
    });
  
    return button.render();   // return button as jquery object
  }






  

//   var modalview=function(context){
//     var $container = options.dialogsInBody ? $(document.body) : $editor;
//     var body = '<div class="form-group">' + '</div>';
//     var footer = '<button href="#" class="btn btn-primary note-examplePlugin-btn">' + lang.myplugin.okButton + '</button>'

//     this.$dialog = ui.dialog({
//  title: lang.examplePlugin.dialogTitle,
//       body: body,
//       footer: footer
      
//        })
//        return modalview.render().appendTo($container);
  

//   }
